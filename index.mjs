import { error } from 'console';
import fs from 'fs';

const start = async () => {
    fs.readFile('./text.txt', 'utf8', (error, data) => {
        if (error)
            console.log(error);
        else {
            console.log("Сначала прочитали файл:", data);
            console.log("Да, это асинхронная функция. Все так и задумано");
        }
    })

    const getRandomText = () => {
        const text = [
            'Text1',
            'Text2',
            'Text3',
            'Text4',
            'Text5'
        ];
        return text[Math.floor(Math.random() * 5)];
    }
    try {
        fs.writeFileSync('./Files/newFile.txt', getRandomText())
        console.log("Потом создан файл")
    } catch (error) {
        fs.mkdirSync('./Files', () => { });
        fs.writeFileSync('./Files/newFile.txt', getRandomText())
        console.log("Потом, т.к. директории не существовало, создали и директорию и файл")
    }
    fs.appendFile('./Files/newFile.txt', "\nДобавочка к добавочке", (error) => {
        if (error) console.log("Не смогли поменять файл :(", error);
        else console.log("Поменяли файл");
    })

    const time = Math.floor(10000 * Math.random());
    setTimeout(() => {
        try {
            console.log('А через ' + time + 'мс:')
            if (fs.existsSync('./Files/newFile.txt')) {
                fs.unlinkSync('./Files/newFile.txt')
                console.log("Снесли файл")
            } else
                console.log("Не снесли файл")
            if (fs.existsSync('./Files/')) {
                fs.rmdirSync('./Files/')
                console.log("Снесли папку")
            } else
                console.log("Не снесли папку")
        } catch (error) { console.log(error) }
    }, time);
}

start();